﻿using System;
using System.Collections.Generic;

// Define the FruitFlyweight class representing the shared intrinsic state (e.g., fruit name).
class FruitFlyweight
{
    public string Name { get; }

    public FruitFlyweight(string name)
    {
        Name = name;
    }
}

// Define the FlyweightFactory class to manage and create flyweight objects.
class FlyweightFactory
{
    private Dictionary<string, FruitFlyweight> flyweights = new Dictionary<string, FruitFlyweight>();

    public FruitFlyweight GetFlyweight(string name)
    {
        if (!flyweights.ContainsKey(name))
        {
            flyweights[name] = new FruitFlyweight(name);
        }
        return flyweights[name];
    }
}

// Define the Fruit class representing the context-specific extrinsic state.
class Fruit
{
    public FruitFlyweight Flyweight { get; }
    public string Color { get; }

    public Fruit(string name, string color, FlyweightFactory factory)
    {
        Flyweight = factory.GetFlyweight(name);
        Color = color;
    }

    public void Display()
    {
        Console.WriteLine($"Name: {Flyweight.Name}, Color: {Color}");
    }
}

class Program
{
    static void Main()
    {
        FlyweightFactory factory = new FlyweightFactory();

        // Create fruit objects with shared flyweight.
        Fruit apple = new Fruit("Apple", "Red", factory);
        Fruit banana = new Fruit("Banana", "Yellow", factory);
        Fruit orange = new Fruit("Orange", "Orange", factory);

        // Display the fruit objects.
        apple.Display();
        banana.Display();
        orange.Display();

        // Check if apple and banana share the same flyweight.
        Console.WriteLine($"Apple and Banana share flyweight: {object.ReferenceEquals(apple.Flyweight, banana.Flyweight)}");
    }
}
